
/*    
    Brian Ton    
    Input.java        
    Version 1.1    
    16 February 2019
*/
import java.util.*;

public final class Input {
    private static final java.util.regex.Pattern intPattern = java.util.regex.Pattern.compile("-?(\\d+)");
    private static final java.util.regex.Pattern doublePattern = java.util.regex.Pattern.compile("-?(\\d*\\.?\\d+)");
    private static final Scanner scanner = new Scanner(System.in);

    private Input() {
    }

    public static String strInput(String prompt) {
        System.out.print(prompt);
        return scanner.nextLine();
    }

    public static int intInp(String prompt) {
        while (true) {
            System.out.print(prompt);
            String in = scanner.nextLine();
            java.util.regex.Matcher m = intPattern.matcher(in);
            m.find();
            try {
                return Integer.parseInt(in.substring(m.start(), m.end()));
            } catch (IllegalStateException e) {
                System.out.println("Please Provide A Numeric Input!");
            }
        }
    }

    public static int intInp(String prompt, double lower, double higher) {
        while (true) {
            System.out.print(prompt);
            String in = scanner.nextLine();
            java.util.regex.Matcher m = intPattern.matcher(in);
            m.find();
            try {
                int out = Integer.parseInt(in.substring(m.start(), m.end()));
                if (out > higher || out < lower) {
                    throw new UnsupportedOperationException("Out Of Range!");
                }
                return out;
            } catch (IllegalStateException e) {
                System.out.println("Please Provide A Numeric Input!");
            } catch (UnsupportedOperationException f) {
                System.out.println(f.getMessage());
            }
        }
    }

    public static int intInp(String prompt, double bound, boolean boundIsLowerBound) {
        while (true) {
            System.out.print(prompt);
            String in = scanner.nextLine();
            java.util.regex.Matcher m = intPattern.matcher(in);
            m.find();
            try {
                int out = Integer.parseInt(in.substring(m.start(), m.end()));
                if (boundIsLowerBound) {
                    if (out < bound) {
                        throw new UnsupportedOperationException("Out Of Range!");
                    }
                } else {
                    if (out > bound) {
                        throw new UnsupportedOperationException("Out Of Range!");
                    }
                }
                return out;
            } catch (IllegalStateException e) {
                System.out.println("Please Provide A Numeric Input!");
            } catch (UnsupportedOperationException f) {
                System.out.println(f.getMessage());
            }
        }
    }

    public static double doubleInp(String prompt) {
        while (true) {
            System.out.print(prompt);
            String in = scanner.nextLine();
            java.util.regex.Matcher m = doublePattern.matcher(in);
            m.find();
            try {
                return Double.parseDouble(in.substring(m.start(), m.end()));
            } catch (IllegalStateException e) {
                System.out.println("Please Provide A Numeric Input!");
            }
        }
    }

    public static double doubleInp(String prompt, double lower, double higher) {
        while (true) {
            System.out.print(prompt);
            String in = scanner.nextLine();
            java.util.regex.Matcher m = doublePattern.matcher(in);
            m.find();
            try {
                double out = Double.parseDouble(in.substring(m.start(), m.end()));
                if (out > higher || out < lower) {
                    throw new UnsupportedOperationException("Out Of Range!");
                }
                return out;
            } catch (IllegalStateException e) {
                System.out.println("Please Provide A Numeric Input!");
            } catch (UnsupportedOperationException f) {
                System.out.println(f.getMessage());
            }
        }
    }

    public static double doubleInp(String prompt, double bound, boolean boundIsLowerBound) {
        while (true) {
            System.out.print(prompt);
            String in = scanner.nextLine();
     
            java.util.regex.Matcher m = doublePattern.matcher(in);
            m.find();
     
            try {
                double out = Double.parseDouble(in.substring(m.start(), m.end()));
                if (boundIsLowerBound) {
                    if (out < bound) {
                        throw new UnsupportedOperationException("Out Of Range!");
                    }
                } else {
                    if (out > bound) {
                        throw new UnsupportedOperationException("Out Of Range!");
                    }
                }
                return out;
            } catch (IllegalStateException e) {
                System.out.println("Please Provide A Numeric Input!");
            } catch (UnsupportedOperationException f) {
                System.out.println(f.getMessage());
            }
        }
    }

    public static char charInp(String prompt) {
        while (true) {
            System.out.print(prompt);
            String in = scanner.nextLine();
            if (in.length() != 1) {
                continue;
            } else {
                return in.charAt(0);
            }
        }
    }

    public static char charInp(String prompt, HashSet<Character> allowedCharacters) {
        while (true) {
            System.out.print(prompt);
            String in = scanner.nextLine();
            if (in.length() != 1 || !allowedCharacters.contains(in.charAt(0))) {
                continue;
            } else {
                return in.charAt(0);
            }
        }
    }
}