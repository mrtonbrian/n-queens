import java.util.HashSet;
import java.util.Arrays;

public class NQueen {
    private static HashSet<Character> allowedCharacters = new HashSet<>(Arrays.asList('o', 'a'));
    private static Bitboard queenBitboard = new Bitboard();
    //private static Bitboard attackedBitBoard = new Bitboard();
    private static Masks masks = new Masks();
    private static int N;
    public static void main(String[] args) {
        N = Input.intInp("N [N Must Be In Interval [1, 8]]: ", 1, 8);
        if (Input.charInp("Solution Amount " + 
                          "['o' for 1 solution, 'a' for all solutions]: "
                          , allowedCharacters) == 'o') {
            solveOne();
        } else {
            solveAll(0, 0);
        }
    }

    private static void solveOne() {
        if (solve(0)) {
            queenBitboard.printBoard(N);
        } else {
            System.out.println("No Solution");
        }
    }

    private static boolean solve(int queenCount) {
        //System.out.println(queenCount); 
        if (queenCount == N) {
            return true;
        }

        for (int row = 0; row < N; row++) {
            for (int col = 0; col < N; col++) {
                if (squareValid(row, col)) {
                    placeQueen(row, col);
                    if (solve(queenCount+1)) {
                        return true;
                    }
                    removeQueen(row,col);
                }
            }
        }

        return false;
    }

    private static void solveAll(int queenCount, int row) {
        //System.out.println(queenCount); 
        if (queenCount == N) {
            queenBitboard.printBoard(N);
        }

        for (int col = 0; col < N; col++) {
            if (squareValid(row, col)) {
                placeQueen(row, col);
                solveAll(queenCount+1, row+1);
                // Backtrack In Order To Get All Solutions                    
                removeQueen(row,col);
            }
        }
        return;
    }

    private static void removeQueen(int row, int column) {
        // Adds Queen To Queen BB and Attacked BB
        queenBitboard.clearSquare(masks, row, column, N);
    }

    private static void placeQueen(int row, int column) {
        // Adds Queen To Queen BB and Attacked BB
        queenBitboard.setSquare(masks, row, column, N);
    }

    private static boolean squareValid(int row, int column) {
        // Sets File
        for (int rowIndex = 0; rowIndex < N; rowIndex++) {
            if (queenBitboard.getBit(rowIndex, column, N)) {
                return false;
            }
        }

        // Sets Rank
        for (int colIndex = 0; colIndex < N; colIndex++) {
            if (queenBitboard.getBit(row, colIndex, N)) {
                return false;
            }
        }
        
        int workingRowVal = row;
        int workingColVal = column;
        // Sets Diagonal Going Up And To Left
        while (workingRowVal >= 0 && workingColVal >= 0) {
            if (queenBitboard.getBit(workingRowVal, workingColVal, N)) {
                return false;
            }
            // Moves "Cursor"
            workingColVal--;
            workingRowVal--;
        }

        workingRowVal = row;
        workingColVal = column;
        // Sets Diagonal Going Down And To Right
        while (workingRowVal < N && workingColVal < N) {
            if (queenBitboard.getBit(workingRowVal, workingColVal, N)) {
                return false;
            }
            // Moves "Cursor"
            workingColVal++;
            workingRowVal++;
        }

        workingRowVal = row;
        workingColVal = column;
        // Sets Diagonal Going Down And To Left
        while (workingRowVal < N && workingColVal >= 0) {
            if (queenBitboard.getBit(workingRowVal, workingColVal, N)) {
                return false;
            }
            // Moves "Cursor"
            workingRowVal++;
            workingColVal--;
        }
        
        workingRowVal = row;
        workingColVal = column;
        // Sets Diagonal Going Up And To Right
        while (workingRowVal >= 0 && workingColVal < N) {
            if (queenBitboard.getBit(workingRowVal, workingColVal, N)) {
                return false;
            }
            // Moves "Cursor"
            workingRowVal--;
            workingColVal++;
        }

        return true;
    }
}
